package com.example.pagingcomponent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.vaadin.pagingcomponent.ComponentsManager;
import org.vaadin.pagingcomponent.Page;
import org.vaadin.pagingcomponent.PagingComponent;
import org.vaadin.pagingcomponent.RangeDisplayer;
import org.vaadin.pagingcomponent.builder.ElementsBuilder;
import org.vaadin.pagingcomponent.button.ButtonPageNavigator;
import org.vaadin.pagingcomponent.customizer.adaptator.GlobalCustomizer;
import org.vaadin.pagingcomponent.customizer.adaptator.impl.CssCustomizerAdaptator;
import org.vaadin.pagingcomponent.customizer.style.StyleCustomizer;
import org.vaadin.pagingcomponent.listener.impl.LazyPagingComponentListener;
import org.vaadin.pagingcomponent.listener.impl.SimplePagingComponentListener;
import org.vaadin.pagingcomponent.utilities.FakeList;

import com.example.pagingcomponent.dao.ProductDao;
import com.example.pagingcomponent.entities.Product;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;

/**
 * Some examples of the {@link PagingComponent} use
 */
@Theme("myTheme")
@SuppressWarnings("serial")
public class PagingComponentApplication extends UI {

	private static final long serialVersionUID = -635051269649061957L;

	@Override
	protected void init(VaadinRequest request) {
		VerticalLayout verticalContent = new VerticalLayout();

		verticalContent.addComponent(integerExemple());
		verticalContent.addComponent(daoExampleWithLazyLoading());
		verticalContent.addComponent(daoExampleWithLoadingOfAllProducts());
		verticalContent.addComponent(styleExempleWithPagingComponentMethods());
		verticalContent.addComponent(styleExempleWithStyleCustomizer());
		verticalContent.addComponent(styleExempleWithCustomizerAdaptator());
		verticalContent.addComponent(styleExempleWithCssCustomizerAdaptator());
		verticalContent.addComponent(rangeDisplayerExemple());
		verticalContent.addComponent(programmingNavigation());

		setContent(verticalContent);
	}

	/** Simple example with integers stored by the {@link PagingComponent}. */
	private VerticalLayout integerExemple() {
		// Test data
		List<Integer> items = createItems();

		final VerticalLayout mainLayout = new VerticalLayout();

		// Layout where we will display items (changing when we click next page).
		final VerticalLayout itemsArea = new VerticalLayout();

		// Visual controls (First, Previous, 1 2 ..., Next, Last)
		final PagingComponent<Integer> pagingComponent = PagingComponent.paginate(items).addListener(new SimplePagingComponentListener<Integer>(itemsArea) {

			@Override
			protected Component displayItem(int index, Integer item) {
				return new Label(String.valueOf(item));
			}

		}).build();

		mainLayout.addComponent(new Label("<h1>Simple example with integers<h1>", ContentMode.HTML));
		mainLayout.addComponent(itemsArea);
		mainLayout.addComponent(pagingComponent);
		return mainLayout;
	}

	/**
	 * Product example with Dao 1 , with the primary keys (Long id) held by the {@link PagingComponent}.
	 */
	private VerticalLayout daoExampleWithLazyLoading() {

		final ProductDao productDao = new ProductDao();

		int nbrOfProducts = productDao.countAllProducts();

		// Create a fake list with the number of products
		List<Product> list = new FakeList<Product>(nbrOfProducts);

		final VerticalLayout mainLayout = new VerticalLayout();

		// Layout where we will display items (changing when we click next page).
		final VerticalLayout itemsArea = new VerticalLayout();

		// Visual controls (First, Previous, 1 2 ..., Next, Last)
		// We use here a LazyPagingComponentListener to fetch the list of items to display from the DB
		final PagingComponent<Product> pagingComponent = PagingComponent.paginate(list).addListener(new LazyPagingComponentListener<Product>(itemsArea) {

			@Override
			protected Collection<Product> getItemsList(int startIndex, int endIndex) {
				// Here we can load the items from the DB
				return productDao.getProductsFromRange(startIndex, endIndex);
			}

			@Override
			protected Component displayItem(int index, Product item) {
				return new Label(item.toString());
			}

		}).build();

		mainLayout.addComponent(new Label("<h1>Example with Dao and lazy loading<h1>", ContentMode.HTML));
		mainLayout.addComponent(itemsArea);
		mainLayout.addComponent(pagingComponent);
		return mainLayout;
	}

	/**
	 * Product example with Dao, with the product held by the {@link PagingComponent}.
	 */
	private VerticalLayout daoExampleWithLoadingOfAllProducts() {

		final ProductDao productDao = new ProductDao();

		final List<Product> productList = productDao.getAllProducts();

		final VerticalLayout mainLayout = new VerticalLayout();

		// Layout where we will display items (changing when we click next page).
		final VerticalLayout itemsArea = new VerticalLayout();

		// Visual controls (First, Previous, 1 2 ..., Next, Last)
		final PagingComponent<Product> pagingComponent = PagingComponent.paginate(productList).addListener(new SimplePagingComponentListener<Product>(itemsArea) {

			@Override
			protected Component displayItem(int index, Product item) {
				return new Label(item.toString());
			}

		}).build(); 

		mainLayout.addComponent(new Label("<h1>Example with Dao that load every products<h1>", ContentMode.HTML));
		mainLayout.addComponent(itemsArea);
		mainLayout.addComponent(pagingComponent);
		return mainLayout;
	}

	/**
	 * Note: these feature are deprecated. You can see the alternatives in {@link #styleExempleWithStyleCustomizer()}, {@link #styleExempleWithCustomizerAdaptator()} and {@link #styleExempleWithCssCustomizerAdaptator()}.
	 * 
	 * Example that demonstrates how to manipulate {@link PagingComponent} styles.
	 */
	@SuppressWarnings("deprecation")
	private VerticalLayout styleExempleWithPagingComponentMethods() {
		// Test data
		List<Integer> items = createItems();

		final VerticalLayout mainLayout = new VerticalLayout();

		// Layout where we will display items (changing when we click next page).
		final VerticalLayout itemsArea = new VerticalLayout();

		// Visual controls (First, Previous,1 2 ..., Next, Last)
		final PagingComponent<Integer> pagingComponent = new PagingComponent<Integer>(10, 9, items, new SimplePagingComponentListener<Integer>(itemsArea) {

			@Override
			protected Component displayItem(int index, Integer item) {
				return new Label(String.valueOf(item));
			}

		});

		// Set the style for all buttons
		pagingComponent.setStyleNameForAllButtons(BaseTheme.BUTTON_LINK);
		// Change the color
		pagingComponent.addStyleNameForAllButtons("styleRed");
		// Add the style to the button of current page
		pagingComponent.addStyleNameCurrentButtonState("styleBold");

		mainLayout.addComponent(new Label("<h1>Example PagingComponent style methods<h1>", ContentMode.HTML));
		mainLayout.addComponent(itemsArea);
		mainLayout.addComponent(pagingComponent);
		return mainLayout;
	}

	/**
	 * Example that demonstrates how to manipulate {@link PagingComponent} styles with the {@link StyleCustomizer}
	 */
	private VerticalLayout styleExempleWithStyleCustomizer() {
		// Test data
		List<Integer> items = createItems();

		final VerticalLayout mainLayout = new VerticalLayout();

		// Layout where we will display items (changing when we click next page).
		final VerticalLayout itemsArea = new VerticalLayout();

		// This customizer allow to add style for each buttons page
		StyleCustomizer styler = new StyleCustomizer() {

			@Override
			public void styleButtonPageNormal(ButtonPageNavigator button, int pageNumber) {
				button.setPage(pageNumber);
				button.removeStyleName("styleRed");
			}

			@Override
			public void styleButtonPageCurrentPage(ButtonPageNavigator button, int pageNumber) {
				button.setPage(pageNumber, "[" + pageNumber + "]"); // Set caption of the button with the page number between brackets.
				button.addStyleName("styleRed");
				button.focus();
			}

			@Override
			public void styleTheOthersElements(ComponentsManager manager, ElementsBuilder builder) {
				// If the number of pages is less than 2, the other buttons (first button, previous button, first separator, ...) are not created.
				if (manager.getNumberTotalOfPages() < 2) {
					return;
				}

				// Allow to hide these buttons when the first page is selected
				boolean visible = !manager.isFirstPage();
				builder.getButtonFirst().setVisible(visible);
				builder.getButtonPrevious().setVisible(visible);
				builder.getFirstSeparator().setVisible(visible);

				// Allow to hide these buttons when the last page is selected
				visible = !manager.isLastPage();
				builder.getButtonLast().setVisible(visible);
				builder.getButtonNext().setVisible(visible);
				builder.getLastSeparator().setVisible(visible);
			}

		};

		// Visual controls (First, Previous,1 2 ..., Next, Last)
		final PagingComponent<Integer> pagingComponent = PagingComponent.paginate(items)
				.numberOfItemsPerPage(25)
				.numberOfButtonsPage(10)
				.styleCustomizer(styler)
				// .elementsCustomizer(myElementsCustomizer) you can also use this method to customize the creation of your components (Buttons, separators, ...).
				.addListener(new SimplePagingComponentListener<Integer>(itemsArea) {

					@Override
					protected Component displayItem(int index, Integer item) {
						return new Label(String.valueOf(item));
					}

				}).build();

		mainLayout.addComponent(new Label("<h1>Example PagingComponent Styles with the StyleCustomizer<h1>", ContentMode.HTML));
		mainLayout.addComponent(itemsArea);
		mainLayout.addComponent(pagingComponent);
		return mainLayout;
	}

	/**
	 * Example that demonstrates how to manipulate {@link PagingComponent} styles with the {@link GlobalCustomizer}
	 */
	private VerticalLayout styleExempleWithCustomizerAdaptator() {
		// Test data
		List<Integer> items = createItems();

		final VerticalLayout mainLayout = new VerticalLayout();

		// Layout where we will display items (changing when we click next page).
		final VerticalLayout itemsArea = new VerticalLayout();

		// This customizer allow to add style for each buttons page and create the different buttons
		GlobalCustomizer adaptator = new GlobalCustomizer() {

			@Override
			public Button createButtonFirst() {
				Button button = new Button("<<");
				button.setStyleName(BaseTheme.BUTTON_LINK);
				return button;
			}

			@Override
			public Button createButtonLast() {
				Button button = new Button(">>");
				button.setStyleName(BaseTheme.BUTTON_LINK);
				return button;
			}

			@Override
			public Button createButtonNext() {
				Button button = new Button(">");
				button.setStyleName(BaseTheme.BUTTON_LINK);
				return button;
			}

			@Override
			public Button createButtonPrevious() {
				Button button = new Button("<");
				button.setStyleName(BaseTheme.BUTTON_LINK);
				return button;
			}

			@Override
			public Component createFirstSeparator() {
				return null;
			}

			@Override
			public Component createLastSeparator() {
				return null;
			}

			@Override
			public ButtonPageNavigator createButtonPage() {
				ButtonPageNavigator button = new ButtonPageNavigator();
				button.setStyleName(BaseTheme.BUTTON_LINK);
				return button;
			}

			@Override
			public void styleButtonPageCurrentPage(ButtonPageNavigator button, int pageNumber) {
				button.setPage(pageNumber, "[" + pageNumber + "]");
				button.addStyleName("styleRed");
				button.focus();
			}

			@Override
			public void styleButtonPageNormal(ButtonPageNavigator button, int pageNumber) {
				button.setPage(pageNumber);
				button.removeStyleName("styleRed");
			}

			@Override
			public void styleTheOthersElements(ComponentsManager manager, ElementsBuilder builder) {
				// Do nothing
			}

		};

		// Visual controls (First, Previous,1 2 ..., Next, Last)
		final PagingComponent<Integer> pagingComponent = PagingComponent.paginate(items)
				.numberOfItemsPerPage(25)
				.numberOfButtonsPage(10)
				.globalCustomizer(adaptator)
				.addListener(new SimplePagingComponentListener<Integer>(itemsArea) {
		
					@Override
					protected Component displayItem(int index, Integer item) {
						return new Label(String.valueOf(item));
					}
		
				}).build();

		mainLayout.addComponent(new Label("<h1>Example PagingComponent Styles with the CustomizerAdaptator<h1>", ContentMode.HTML));
		mainLayout.addComponent(itemsArea);
		mainLayout.addComponent(pagingComponent);
		return mainLayout;
	}

	/**
	 * Example that demonstrates how to manipulate {@link PagingComponent} styles with the {@link CssCustomizerAdaptator}
	 */
	private VerticalLayout styleExempleWithCssCustomizerAdaptator() {
		// Test data
		List<Integer> items = createItems();

		final VerticalLayout mainLayout = new VerticalLayout();

		// Layout where we will display items (changing when we click next page).
		final VerticalLayout itemsArea = new VerticalLayout();

		// Visual controls (First, Previous,1 2 ..., Next, Last)
		final PagingComponent<Integer> pagingComponent = PagingComponent.paginate(items)
				.numberOfItemsPerPage(25)
				.numberOfButtonsPage(10)
				.globalCustomizer(new CssCustomizerAdaptator())
				.addListener(new SimplePagingComponentListener<Integer>(itemsArea) {
		
					@Override
					protected Component displayItem(int index, Integer item) {
						return new Label(String.valueOf(item));
					}
		
				}).build();
		pagingComponent.setStyleName("css-style");

		mainLayout.addComponent(new Label("<h1>Example PagingComponent Styles with the CssCustomizerAdaptator<h1>", ContentMode.HTML));
		mainLayout.addComponent(itemsArea);
		mainLayout.addComponent(pagingComponent);
		return mainLayout;
	}

	/**
	 * Example with {@link RangeDisplayer}, i.e. display "Product 20-40 currently displayed".
	 */
	private VerticalLayout rangeDisplayerExemple() {
		// Test data
		List<Integer> items = createItems();

		final VerticalLayout mainLayout = new VerticalLayout();

		// Layout where we will display items (changing when we click next page).
		final VerticalLayout itemsArea = new VerticalLayout();

		// Visual controls (First, Previous,1 2 ..., Next, Last)
		final PagingComponent<Integer> pagingComponent = PagingComponent.paginate(items).addListener(new SimplePagingComponentListener<Integer>(itemsArea) {

			@Override
			protected Component displayItem(int index, Integer item) {
				return new Label(String.valueOf(item));
			}

		}).build();

		// We declare a RangeDisplayer which takes PagingComponent as parameter
		RangeDisplayer<Integer> rd = new RangeDisplayer<Integer>(pagingComponent);
		// We set the value to be displayed before range ex : results : 10-20
		// It's optional
		rd.setValue("results : ");

		mainLayout.addComponent(new Label("<h1>Example with RangeDisplayer<h1>", ContentMode.HTML));
		mainLayout.addComponent(itemsArea);
		mainLayout.addComponent(pagingComponent);
		mainLayout.addComponent(rd);
		return mainLayout;
	}
	
	/** Navigate between pages by programming */
	private VerticalLayout programmingNavigation() {
		// Test data
		List<Integer> items = createItems();
		
		// Layout where we will display items (changing when we click next page).
		final VerticalLayout itemsArea = new VerticalLayout();

		// Visual controls (First, Previous, 1 2 ..., Next, Last)
		final PagingComponent<Integer> pagingComponent = PagingComponent.paginate(items).addListener(new SimplePagingComponentListener<Integer>(itemsArea) {

			@Override
			protected Component displayItem(int index, Integer item) {
				return new Label(String.valueOf(item));
			}

		}).build();
		pagingComponent.setVisible(false);
		
		final TextField pageField = new TextField();
		final Label errorLabel = new Label();
		final Button buttonGo = new Button("GO", new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				try {
					pagingComponent.go(Integer.valueOf(pageField.getValue()));
					errorLabel.setValue("");
				} catch (IndexOutOfBoundsException e) {
					errorLabel.setValue("this page number is invalable");
				} catch (Exception e) {
					errorLabel.setValue("this page number is invalid");
				}
			}
		});
		final Button buttonGoFirstLastPage = new Button("Go last page", new Button.ClickListener() {
			
			private boolean goFirstPage;
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (goFirstPage) {
					event.getButton().setCaption("Go last page");
					pagingComponent.go(Page.FIRST);
				} else {
					event.getButton().setCaption("Go first page");
					pagingComponent.go(Page.LAST);
				}
				
				goFirstPage=!goFirstPage;
			}
		});
		
		HorizontalLayout programmingNavigationLayout = new HorizontalLayout();
		programmingNavigationLayout.addComponent(pageField);
		programmingNavigationLayout.addComponent(buttonGo);
		programmingNavigationLayout.addComponent(errorLabel);
		programmingNavigationLayout.addComponent(buttonGoFirstLastPage);

		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.addComponent(new Label("<h1>Programming navigation<h1>", ContentMode.HTML));
		mainLayout.addComponent(programmingNavigationLayout);
		mainLayout.addComponent(itemsArea);
		mainLayout.addComponent(pagingComponent);
		return mainLayout;
	}

	/**
	 * @return a list of Integers
	 */
	private List<Integer> createItems() {
		List<Integer> items = new ArrayList<Integer>();
		for (Integer i = 1; i <= 300; i++) {
			items.add(i);
		}

		return items;
	}

}
