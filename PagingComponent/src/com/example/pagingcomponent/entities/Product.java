package com.example.pagingcomponent.entities;

import java.io.Serializable;

/**
 * Fake entity for the examples.
 */
public class Product implements Serializable {

	private static final long serialVersionUID = -1645334961373749274L;
	
	private Long id;
    private String name;
    private int price;
    
    public Product(Long id,String name, int price){
        this.id = id;
        this.name = name;
        this.price = price;
    }
    
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getPrice() {
        return price;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }
    
    public Long getId() {
        return id;
    }
    
    @Override
    public String toString() {
        return name+": "+price+" $";
    }
}
