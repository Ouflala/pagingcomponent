package com.example.pagingcomponent.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.example.pagingcomponent.entities.Product;


/** Fake DAO (holding a list of products with no DB) to stimulate the usage of a classic DAO with a DB */
public class ProductDao implements Serializable {
	
	private static final long serialVersionUID = -2364764071377555924L;
	
	private List<Product> listProducts;

    public ProductDao(){
        // Construct fake list of Products
        listProducts=new ArrayList<Product>();
        for(int i=1;i<=100;i++){
            listProducts.add(new Product(new Long(i),"Product "+i, i*10));
        }
    }
    
    /** Returns the IDs of all the products in the "DB" */
    public List<Long> getProductIds(){
        List<Long> listIds=new ArrayList<Long>();
        
        for(Product product:listProducts){
            listIds.add(product.getId());
        }
        
        return listIds;
    }
    
    /** Returns all the products of the "DB" */
    public List<Product> getAllProducts(){
        return listProducts;
    }
    
    /** Returns the number of products of the "DB" */
    public int countAllProducts() {
    	return listProducts.size();
    }
    
    /** Returns a "page" of the resultSet (if we had a DB) */
    public List<Product> getProductsFromRange(int indexStart, int indexEnd){
        return listProducts.subList(indexStart, indexEnd);
    }
}
