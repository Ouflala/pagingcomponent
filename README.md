This addon is for the java framework web Vaadin : https://vaadin.com

You can download it at https://vaadin.com/addon/pagingcomponent

You may have too much data to display on the page. For example, you have a search page and you only want to display the 20 first results. But the user must be able to browse the rest of the results. This add-on will very easily enable you to place numbers of pages to jump to.

You do not need to use a Table to use PagingComponent.

The examples can be found in the PagingComponentApplication class.

This is used in production in the BlackBeltFactory.com platform, in the forum and the course search result page.